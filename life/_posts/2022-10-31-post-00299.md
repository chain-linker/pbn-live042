---
layout: post
title: "동탄 파크릭스 A51-1BL, A51-2BL, A52BL 분양가 분석, 평면도, 청약 정보"
toc: true
---


## 동탄 파크릭스 A51-1BL, A51-2BL, A52BL 분양가 분석, 평면도, 청약 정보

## 전용면적, 공급면적 및 평당 분양가 갈무리 표

### ※발코니 확장 입비 포함한 분양가 비교
- 74타입 : 최저 4.33억원 ~ 최상 4.65억원 [최저 평당 1,498만원 ~ 최고 평당 1,609만원]
- 84A타입 : 최저 4.95억원 ~ 최고 5.32억원 [최저 평당 1,516만원 ~ 최고 평당 1,627만원]
- 84B타입 : 최저 4.90억원 ~ 최상 5.26억원 [최저 평당 1,500만원 ~ 최고 평당 1,610만원]
- 97A타입 : 최저 5.94억원 ~ 최고 6.38억원 [최저 평당 1,580만원 ~ 최고 평당 1,697만원]
- 97B타입 : 최저 5.94억원 ~ 최상 6.32억원 [최저 평당 1,567만원 ~ 최고 평당 1,665만원]
- 110A타입 : 최저 6.83억원 ~ 최고 7.26억원 [최저 평당 1,616만원 ~ 최고 평당 1,718만원]
- 110P1타입 : 최저 8.32억원 ~ 최고 8.32억원 [최저 평당 1,912만원 ~ 최고 평당 1,912만원]
- 110P2타입 : 최저 8.40억원 ~ 최고 8.40억원 [최저 평당 1,961만원 ~ 최고 평당 1,961만원]
 

## 전용면적, 공급면적 및 평당 분양가 조치 표

### ※발코니 확장 비발 포함한 분양가 비교
- 74타입 : 최저 4.34억원 ~ 최상 4.66억원 [최저 평당 1,493만원 ~ 최고 평당 1,603만원]
- 84A타입 : 최저 4.98억원 ~ 최고 5.35억원 [최저 평당 1,515만원 ~ 최고 평당 1,626만원]
- 84B타입 : 최저 4.92억원 ~ 최상 5.28억원 [최저 평당 1,495만원 ~ 최고 평당 1,604만원]
- 97A타입 : 최저 5.95억원 ~ 최상 6.39억원 [최저 평당 1,575만원 ~ 최고 평당 1,692만원]
- 97B타입 : 최저 5.96억원 ~ 최상 6.33억원 [최저 평당 1,561만원 ~ 최고 평당 1,659만원]
- 110A타입 : 최저 6.86억원 ~ 최상 7.30억원 [최저 평당 1,614만원 ~ 최고 평당 1,717만원]
- 110P1타입 : 최저 8.38억원 ~ 최고 8.38억원 [최저 평당 1,916만원 ~ 최고 평당 1,916만원]
- 110P2타입 : 최저 8.44억원 ~ 최고 8.44억원 [최저 평당 1,957만원 ~ 최고 평당 1,957만원]
 

## 전용면적, 공급면적 및 평당 분양가 조처 표

### ※발코니 확장 비용 포함한 분양가 비교
- 74타입 : 최저 4.29억원 ~ 최고 1,586억원 [최저 평당 1,478만원 ~ 최고 평당 1,586만원]
- 84A타입 : 최저 4.92억원 ~ 최상 1,610억원 [최저 평당 1,500만원 ~ 최고 평당 1,610만원]
- 84B타입 : 최저 4.85억원 ~ 최상 1,588억원 [최저 평당 1,479만원 ~ 최고 평당 1,588만원]
- 97A타입 : 최저 5.88억원 ~ 최상 1,674억원 [최저 평당 1,559만원 ~ 최고 평당 1,674만원]
- 97B타입 : 최저 5.82억원 ~ 최상 1,642억원 [최저 평당 1,529만원 ~ 최고 평당 1,642만원]
- 110A타입 : 최저 6.81억원 ~ 최고 1,723억원 [최저 평당 1,604만원 ~ 최고 평당 1,723만원]
- 110P1타입 : 최저 8.27억원 ~ 최상 1,894억원 [최저 평당 1,894만원 ~ 최고 평당 1,894만원]
- 110P2타입 : 최저 8.34억원 ~ 최상 1,938억원 [최저 평당 1,938만원 ~ 최고 평당 1,938만원]

## 조감도 및 개요

 

## 청약 접수 관계 조력 및 조건
 - 상관 주택건설지역(경기도 화성시 동탄2택지개발지구)은 투기과열지구 및 청약과열지역으로서, 본 아파트는 2주택 종말 소유한 세대에 속한 분은 1순위 자격에서 제외됩니다.
(무주택 세대 또는 1주택을 소유한 세대에 속한 분만 1순위 청약 가능)
- 계열 주택건설지역(경기도 화성시 동탄2택지개발지구)은 투기과열지구 및 청약과열지역으로서, 본 아파트는 1주택 야심 소유한 세대에 속한 분은 1순위 가점제 청약이 불가하며, 2주택 이상 소유한 세대에 속한 분은 1순위 자격에서 제외됩니다.
- 본 아파트는 수도권 투기과열지구 및 청약과열지역의 대규모 택지개발지구에서 공급하는 분양가상한제 실용 민영주택으로, 본 아파트의 당첨자로 선정된 분 및 현 세대에 속한 분은 재당첨 제한기간(당첨일로부터 10년간) 동안 다른 분양주택(분양전환공공임대주택을 포함하되, 투기과열지구 및 청약과열지역이 아닌 지역에서 공급되는 민영주택은 제외)의 입주자(사전당첨자 포함)로 선정될 수 없습니다.
- 본 아파트의 당첨자로 선정 아리아 당첨자 및 세대에 속한 자는 당첨일로부터 향후 5년간 투기과열지구 및 청약과열지역에서 공급하는 주택의 1순위 청약접수가 제한되오니 유의하시기 바랍니다.

- 본 아파트는 궐초 입주자모집공고일(2022.11.04.) 인제 화성시에 거주하거나 수도권(서울특별시, 인천광역시, 경기도)에 거주(주민등록표등본 기준)하는 만19세 이상인 자식새끼 또는 세대주인 미성년자의 경우 청약이 가능합니다.
다만 청약 신청자 중 같은 순위 내에 경쟁이 있을 경우 해당 주택건설지역인 화성시 2년 이상 계속 거주자가 우선합니다.
- 본 아파트는「주택공급에 관한 규칙」제34조에 요인 대규모 택지개발지구에 공급하는 주택으로 입주자모집공고일(2022.11.04.) 방금 화성시에 2년 압미 유지 거주한 자[2020.11.04. 이전부터 부절 거주]에게 일반공급 세대의 30%를 우선
공급하며, 경기도에 2년 공상 구속 거주한 자[2020.11.04. 이전부터 속박 거주자]에게 20%를 공급(화성시 공급신청자 공급물량이 미달될 처지 경기도 2년 극처 연장 거주한 아드님 공급물량에 포함)하며 50%를 수도권거주자(서울특별시,인천광역시,경기도 2년 미만 거주한 자)에게 공급 (경기도 2년 이상 계속 거주 공급신청자 공급물량이 미달될 경우 수도권 거주자 공급물량에 포함)합니다.

## 전매 최종점 기간입니다

### - 특별공급 : 입주자로 선정된 날부터 10년까지 전매가 금지됩니다.
- 일반공급 : 입주자로 선정된 날부터 10년까지 전매가 금지됩니다.
 

## 분양가를 납부하는 조건입니다.

### - 계약금 : 분양 가격의 10%입니다.
- 중도금 : 분양 가격의 60%입니다.
- 잔금 : 분양 가격의 30%입니다.
(중도금 대출은 변리 후불제 조건으로 진행될 예정입니다.)
 

## A51-1BL 오직 규모

### - 지하 최저 2층부터 세속 최상 20층까지이며 8개동으로 설계되어 있습니다.
- 세대수는 총 310세대입니다.
[특별공급 147세대(일반[기관추천] 23세대, 다자녀가구 31세대, 신혼부부 42세대, 노부모부양 9세대, 생애최초 42세대 포함)]

 

## A51-2BL 고작 규모

### - 지하 최저 2층부터 인심 최상 20층까지이며 9개동으로 설계되어 있습니다.
- 세대수는 총 414세대입니다.
[특별공급 193세대(일반[기관추천] 28세대, 다자녀가구 41세대, 신혼부부 56세대, 노부모부양 12세대, 생애최초 56세대 포함)]

 

## A52BL 다다 규모

### - 지하 최저 1층부터 인심 최상 20층까지이며 13개동으로 설계되어 있습니다.
- 세대수는 총 세대입니다.
[특별공급 315세대(일반[기관추천] 46세대, 다자녀가구 68세대, 신혼부부 91세대, 노부모부양 19세대, 생애최초 91세대 포함)]
 

## 입주 예정일

### - 2025년 7월로 예정되어 있습니다.

### 

## 청약 신청 일정

### 1. 특별공급 날짜 [동탄 파크릭스](https://attention-collect.com/life/post-00025.html) : 2022년 11월 14일(월요일)입니다.
2. 일반공급 1순위 날짜 : 2022년 11월 15일(화요일)입니다. 시간은 09:00~17:30입니다.
3. 일반공급 2순위 날짜 : 2022년 11월 16일(수요일)입니다. 시간은 09:00~17:30입니다.

### 

## 당첨자 발표일

### - 2022년 11월 22일(화요일)입니다.(청약 Home에서 조회 가능)
 

## 당첨자 발표일

### - 2022년 11월 24일(목요일)입니다.(청약 Home에서 조회 가능)
 

## 당첨자 발표일

### - 2022년 11월 25일(금요일)입니다.(청약 Home에서 조회 가능)
 

## 당첨자 기록 제출 기간입니다. (계약 체결 전 서류심사)

### - 2022년 11월 26일(토요일)~2022년 12월 2일(금요일)입니다.
- 기능 : 구비 서류를 지참하여 견본주택을 방문하여 접수하시면 됩니다.

### 

## 서약 체결 일정 및 장소

### - 2022년 12월 8일(목요일)~2022년 12월 12일(월요일),
- 방도 : 견본주택을 방문하여 계약을 체결하시면 됩니다.
 

## A51-1BL 분양가 온통 표입니다.

 

## A51-2BL 분양가 전체 표입니다.

 

## A52BL 분양가 전체 표입니다.



 

## 동탄 파크릭스 A51-1BL, A51-2BL, A52BL 분양가 분양 일정

 

## 동탄 파크릭스 A51-1BL, A51-2BL, A52BL 분양가 입지환경

 

## 동탄 파크릭스 A51-1BL, A51-2BL, A52BL 분양가 프리미엄



 

## 동탄 파크릭스 A51-1BL, A51-2BL, A52BL 분양가 단지 기도 및 시스템

 

## 동탄 파크릭스 A51-1BL, A51-2BL, A52BL 분양가 커뮤니티



 

## 74타입 아파트 평면도

 

## 84A타입 아파트 평면도

 

## 84B타입 아파트 평면도



 

## 97A타입 아파트 평면도

 

## 97B타입 아파트 평면도



 

## 110A타입 아파트 평면도

 

## 110P1타입 아파트 평면도

 

## 110P2타입 아파트 평면도



 

## 아파트 단지배치도



 

## 모표 하우스(견본주택) 약도 및 안내

 

## 동탄 파크릭스 A51-1BL, A51-2BL, A52BL 분양가 입주자모집공고 다운로드

 

 

## 부동산 첩정 의사 카페입니다.

### cafe.naver.com/realestatereal

## 부동산 정보 소갈딱지 방입니다.(*비밀번호:8822)

### https://open.kakao.com/o/guXQEYsc

### 

